﻿/*
Pipeable.h
Contains definitions for the Pipeable library:
Class Pipeable - TODO document
Factory function make_pipeable - TODO document

Author: Luka Prebil Grintal, 2020
*/

#pragma once
namespace lpg {


template<typename Input>
class Pipeable {
private:
	Input _value;

public:
	Pipeable(Input const& value) : _value(value) {};

	template<typename Func>
	auto operator|(Func operation) -> Pipeable<decltype(operation(std::declval<Input>()))>;

	operator Input() const {
		return this->_value;
	}
};

template<typename Input>
template<typename Func>
auto Pipeable<Input>::operator|(Func operation) -> Pipeable<decltype(operation(std::declval<Input>()))> {
	auto value = operation(this->_value);
	return Pipeable<decltype(operation(std::declval<Input>()))>{value};
}

template<typename T>
Pipeable<T> make_pipeable(T const& value) {
	return Pipeable{value};
}


} // END namespace lpg