#include <gtest/gtest.h>
#include <string>
#include <optional>
#include <vector>
#include <algorithm>
#include <numeric>

#include "Pipeable.h"

// Helpers until we can pass parameters to stages
namespace helpers {
    std::vector<int> square(std::vector<int> const& in) {
        std::vector<int> ret(in.size());
        std::transform(cbegin(in), cend(in), begin(ret), [](auto const& element) { return element * element; });

        return ret;
    }

    int sum(std::vector<int> const& in) {
        return std::accumulate(cbegin(in), cend(in), 0);
    }

    std::string parse_opt(std::optional<std::string> opti) {
        return opti.value_or("empty");
    }
}

TEST(MAKE_PIPEABLE, Should_return_Pipeable) {
    std::string input = "Some input";

    EXPECT_NO_THROW( lpg::Pipeable<std::string> result = lpg::make_pipeable(input) );
}

TEST(PIPEABLE, Should_allow_chaining_callables) {
    std::string input = "asdf";

    EXPECT_NO_THROW( 
        lpg::make_pipeable(std::optional{input}) 
            | helpers::parse_opt 
            | [](auto str) { return str.length(); }
    );
}

TEST(PIPEABLE, Pipeable_T_should_convert_to_T) {
    std::string input = "asdf";

    EXPECT_NO_THROW( 
        size_t result = 
        lpg::make_pipeable(std::optional{input}) 
            | helpers::parse_opt 
            | [](auto str) { return str.length(); }
    );

    EXPECT_NO_THROW( 
        std::string str_result = 
        lpg::make_pipeable(std::optional<std::string>{std::nullopt}) 
            | helpers::parse_opt
    );
}

TEST(PIPEABLE, Implicit_conversion_to_T_should_return_final_result) {
    std::string const input = "#include";

    std::string const result = lpg::make_pipeable(input)
        | [](auto str) { return str.append("<"); }
        | [](auto str) { return str.append("C++"); }
        | [](auto str) { return str.append(">"); };

    EXPECT_EQ(result, "#include<C++>"); // Result is correct
    EXPECT_EQ(input, "#include"); // Input was not changed

}

TEST(PIPEABLE, Transform_Reduce_vector) {
    std::vector<int> input(50);
    std::iota(begin(input), end(input), 0);

    int result = lpg::make_pipeable(input)
        | helpers::square // TODO make these std::transform
        | helpers::sum;   // and std::accumulate

    EXPECT_EQ(result, 40425);
}