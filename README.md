# Pipeable

Library for generating pipelines of callables working on some input data.

### Examples

```cpp
#include <cmath>
#include <iostream>

#include "Pipeable.h"

using namespace std; // Don't do this in real code, ever.

int main() {
    double input = 42.2;
    
    double output = lpg::make_pipeable(input)
        | floor
        | sqrt
        | log;
    
    cout << output << endl;
}
```